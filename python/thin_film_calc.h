#ifndef thin_film_calc_H
#define thin_film_calc_H

#include "aligned_vector.h"
#include <vector>
#include <complex>
#include <random>


template <class T>
using aligned_vector = std::vector<T, alligned_allocator<T, 64>>;


/*
The physical symbols used are based on:
Prof. Falk Lederer, Institute for Solid State Theory and Theoretical Optics Friedrich Schiller University Jena:
https://www.iap.uni-jena.de/iapmedia/de/Lecture/Grundkonzepte+der+Optik1414710000/GdO_SS2014_Optikskript_Lederer_1999WS.pdf
 */




// ###################################################################################################
// Helper functions to build up input data for the test scenarios - these were !! Not !! optimized
// ###################################################################################################


/*** multiplies each entry of input_vec by omega_c0 and writes the result to output_vec, is only used to set up the tests
 *
 * @param input_vec    Vector from complex numbers complex <double> {refractive index, absorption}
 * @param num_schicht  length of input_vec
 * @param omega_c0     physical constant
 * @param output_vec   output
 */
void calc_k_ix_omega_c0 (std::complex<double> * __restrict__ input_vec,
                         const int num_schicht ,
                         const double omega_c0 ,
                         std::complex<double> * __restrict__  output_vec  ) ;


/*** like numpy linespace -> outputs double vector with equidistant digits between lower and upper, the number of which corresponds to num_steps, is only used to set up the tests
 *
 * @param num_steps count of generated numbers
 * @param lower     Lower limit of the interval
 * @param upper     upper limit of the interval
 * @return          returns vector of equidistant points in the interval [lower, upper]
 */
aligned_vector<double> linespace_wavelength(int num_steps,
                                            int lower,
                                            int upper);



/***  Builds data structures for test scenario in main ()
 *
 * @param wavelength        Vector of the wavelengths at which the reflectivity is to be calculated later
 * @param vec_brechzahl     Vector of the refractive index of the materials used for each wavelength
 * @param vec_absorb        Vector of the absorption of the materials used for each wavelength
 * @param num_stuetz        Number of support points -> corresponds to the length of the vector wavelength
 * @param num_schicht       Number of layers in the layer system
 */
void build_data (double * __restrict__ wavelength,
                 double * __restrict__ vec_brechzahl,
                 double * __restrict__ vec_absorb,
                 const int num_stuetz,
                 const int num_schicht);




// ################################################################################################
// functions for the actual calculations or the actual project
// ################################################################################################




/***  Calculates complex 2x2 matrix * matrix product and writes results in A. Corresponds to: A * = B
 *    is used in function: calc_TransferMatrix, calc_TransferMatrix_precomp
 *    result is written to A
 *
 * @param A     Vector of the complex 2x2 matrix A
 * @param B     Vector of the complex 2x2 matrix B
 */
void matrix_product (std::complex<double>  * __restrict__ A,
                     const std::complex<double> * __restrict__ B);

/*** Precomputes all possible complex 2x2 matrices and stores them for later caching in vector precomp_maticies
 *
 * @param k_ix_omega_c0         Material properties, complex<double> vector of refraction indeses and absorbtion multiplied by omega_co
 * @param dn_thickness          vector of thickness of the corresponding materials
 * @param num_mat               number of materials used for coating
 * @param num_stuetz            number of wavelength, where to compute the reflectivity
 * @param num_dick              number of discrete thickness values
 * @param precomp_maticies      memory to store the calculated matrices
 */
void calc_interims(const std::complex<double> * __restrict__ k_ix_omega_c0 ,
                   const double * __restrict__ dn_thickness,
                   const int num_mat,
                   const int num_stuetz,
                   const int num_dick,
                   std::complex<double> * __restrict__ precomp_maticies);


/*** old version to compute the transfermatrix, is still used for testing
 *
 * @param k_ix_omega_c0         Material properties, complex<double> vector of refraction indeses and absorbtion multiplied by omega_co
 * @param dn_thickness          vector of thickness of the corresponding materials
 * @param tMatrix               memory to store the transfermatrix
 * @param interim               memory for the intermediate results
 * @param num_schicht           number of layers in thin film system
 */
void calc_TransferMatrix(const std::complex<double> * __restrict__ k_ix_omega_c0 , const double * __restrict__ dn_thickness,
                         std::complex<double>  * __restrict__ tMatrix , std::complex<double>  * __restrict__ interim ,
                         const int num_schicht);


/***    Computes the transfer-matrix, using precomputed intermediated complex 2x2 matrices from calc_interims
 *
 * @param precomp_matricies         precomputed complex 2x2 matrices from function calc_interims
 * @param tMatrix                   memory for the transfer-matrix
 * @param rand_dick                 vector of random numbers, indicating which thickness values to used in this iteration
 * @param num_mat                   number of coating materials in thin film system
 * @param num_schicht               number of layers in thin film system
 * @param num_dick                  number of discrete thickness values
 * @param stuetz_id                 id of the wavelength we want to compute
 */
void calc_TransferMatrix_precomp( std::complex<double> * __restrict__ precomp_matricies,
                                  std::complex<double>  * __restrict__ tMatrix,
                                  int *  __restrict__ rand_dick,
                                  const int num_mat,
                                  const int num_schicht,
                                  const int num_dick ,
                                  const int stuetz_id);




/***  Calculates the reflectivity from the tMatrix and the refractive indices of the output medium (k_inx) and the input medium (k_outx)
 *    is used in function thin_film_calculator
 *
 * @param tMatrix   intpur: transfer-matrix
 * @param k_outx    Refractive index input medium (no error! This is the confusing notation of Prof. Lederer)
 * @param k_inx     Refractive index output medium (No mistake! This is the confusing notation of Prof. Lederer)
 * @return          Reflectivity for the wavelength represented by the transfer matrix
 */
double calc_RTP(const std::complex<double> * __restrict__ tMatrix,
                const double k_outx,
                const double k_inx );


/***Checks whether the currently calculated thicknesses are a solution to the problem
 * used in function: thin_film_calculator
 *
 * @param vec_targets       Target values ​​of the desired reflectivity at the wavelengths
 * @param test_solution     memory for intermediate results
 * @param num_schicht       number of layers in thin film system
 * @return                  0 = test_solution is not a solution, 1 = test_solution is a valid solution
 */
bool test_solution(double * __restrict__ vec_targets ,
                   double * __restrict__ test_solution ,
                   int num_schicht);


/*** Main function of the library, searches for a suitable combination of layer thicknesses for a given layer system, so that
 the reflectivity values ​​desired in vec_targets are met
 *
 * @param wavelength                Wavelengths at which the reflectivity should be calculated
 * @param vec_brechzahl             Vector of the refractive index, of the materials used for each wavelength
 * @param vec_absorb                Vector of the absorption, the materials used for each wavelength
 * @param vec_targets               Target values ​​of the desired reflectivity at the wavelengths
 * @param final_solution            allocated memory for the output of the solution, i.e. a combination of the layer thicknesses
 * @param num_stuetz                Number of support points or examined wavelengths corresponds to the length of the vector wavelength
 * @param num_schicht               Number of layers in the layer system
 * @param zwl                       Central wavelength, roughly corresponds to the median of wavelength
 * @param max_num_iterations        maximum number of iterations the search should run
 * @param size_L1_cache_in_KB       size of L1 cache to fit the data to, default is 256 Kilobytes
 */
void thin_film_calculator  (double *  __restrict__ wavelength,
                            double *  __restrict__ vec_brechzahl,
                            double *  __restrict__ vec_absorb,
                            double *  __restrict__ dn_thickness,
                            double *  __restrict__ vec_targets,
                            double *  __restrict__ final_solution,
                            int num_stuetz ,
                            int num_schicht,
                            int num_dick,
                            int num_mat,
                            int zwl,
                            int64_t max_num_iterations,
                            int size_L1_cache_in_KB = 256);
#endif
