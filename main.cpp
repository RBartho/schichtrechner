#include <vector>

#include "PerfEvent.hpp"
#include "thin_film_calc.h"
#include "aligned_vector.h"



using namespace std;
template <class T>
using aligned_vector = std::vector<T, alligned_allocator<T, 64>>;


int main() {
    // create realistic test case
    const double zwl = 1030.0;
    const int num_stuetz = 300;
    const int num_schicht = 30;
    const int num_mat = 2;
    aligned_vector<double> wavelength = linespace_wavelength(num_stuetz, 600, 1400);
    aligned_vector<double> vec_targets (num_stuetz,0.0);  // 0,01 is 1 percent reflectivity,if set to 0.0 no solution is found and all max_num_iterations are computed (for testing)
    uint64_t max_num_iterations = 100000;
    aligned_vector<double> vec_omega (num_stuetz);
    aligned_vector<double> vec_brechzahl ((num_mat+2) * num_stuetz);
    aligned_vector<double> vec_absorb ((num_mat+2) * num_stuetz);
    aligned_vector<double> final_solution (num_schicht,0);


    // choose discretization of search space
    //int num_dick = 12;     // = 230 Kbytes data, fits in L1 cache
    int num_dick = 50;       // = 960 kbytes data, fits in L2 cache
    //int num_dick = 300;    // = 5760 kbytes data,  fits in L3 cache
    //int num_dick = 3000;   //  57600 Kilobytes  to big for Cache

    // Discretize thickness , is  equivalent to discretization of the search space
    aligned_vector <double> dn_thickness(num_dick,0);
    double steps = (3* (zwl/4)) / num_dick;  // this step size has physical reasons
    for (int i = 0 ; i < num_dick ; i++){
        dn_thickness[i] = (i+1) * steps;
    }

    // build input datat
    build_data (wavelength.data(), vec_brechzahl.data(), vec_absorb.data(), num_stuetz, num_mat  );

    // for testing only
    double size_in_kbytes = (num_stuetz * num_dick * num_mat * 4 * 8) / 1000;
    cout << "Does it fit into Cache? Size_of precomp_maticies:  " << size_in_kbytes << " Kilobytes." << endl;
    cout << "Size of L1 Cache in Linux Pool 1 :  256 kilobytes" << endl;
    cout << "Size of L2 Cache in Linux Pool 1 : 1000 kilobytes" << endl;
    cout << "Size of L3 Cache in Linux Pool 1 : 6000 kilobytes" << endl;
    cout << "\n";
    if (size_in_kbytes < 256){
        cout << "--> fits into L1 Cache" << endl;
    }else if (size_in_kbytes < 1000){
        cout << "--> fits into L2 Cache" << endl;
    }else if (size_in_kbytes < 6000){
        cout << "--> fits into L3 Cache" << endl;
    }else if (size_in_kbytes > 6000){
        cout << "--> does not fit into cache" << endl;
    }

    // measure performance
    BenchmarkParameters params;
    bool printHeader=1;  // Print header only first time
    PerfEvent e;
    {
        params.setParam("name", "thin_film_calculator  ");
        PerfEventBlock b(max_num_iterations * num_stuetz, params, printHeader);
        thin_film_calculator  (wavelength.data(),    vec_brechzahl.data(), vec_absorb.data(), dn_thickness.data(), vec_targets.data(), final_solution.data(), \
        num_stuetz ,num_schicht, num_dick, num_mat, zwl, max_num_iterations);
    }

}


