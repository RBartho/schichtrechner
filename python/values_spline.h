
#ifndef values_SPLINE_H
#define values_SPLINE_H

#include <iostream>
#include <vector>
#include "spline.h"

// Werte von https://refractiveindex.info/


struct interpol {
    tk::spline brech_spline;
    tk::spline absorb_spline;
};


interpol interpol_AIR() {
    std::vector <double> brech(20,0);
    std::vector <double> wavelength_brech(20,0);
    std::vector <double> absorb(20,0);
    std::vector <double> wavelength_absorb(20,0);

    // Hard_Code Values
    // Brechzahl
    wavelength_brech[0] =300;
    brech[0] = 1.000291151532;
    wavelength_brech[1] = 405;
    brech[1] = 1.0002824879531;
    wavelength_brech[2] = 507;
    brech[2] = 1.0002787832208;
    wavelength_brech[3] = 609;
    brech[3] = 1.0002768435966;
    wavelength_brech[4] = 711;
    brech[4] = 1.0002756979483;
    wavelength_brech[5] = 814;
    brech[5] = 1.0002749639005;
    wavelength_brech[6] = 901;
    brech[6] = 1.000274525871;
    wavelength_brech[7] = 1004;
    brech[7] = 1.0002741537295;
    wavelength_brech[8] = 1106;
    brech[8] = 1.0002738819616;
    wavelength_brech[9] = 1208;
    brech[9] = 1.0002736766354;
    wavelength_brech[10] = 1310;
    brech[10] = 1.0002735177028;
    wavelength_brech[11] = 1413;
    brech[11] = 1.0002733910563;
    wavelength_brech[12] = 1500;
    brech[12] = 1.0002733038787;
    wavelength_brech[13] = 1602;
    brech[13] = 1.0002732192871;
    wavelength_brech[14] = 1690;
    brech[14] = 1.0002731583221;
    wavelength_brech[15] = 1800;
    brech[15] = 1.0002731583221;
    wavelength_brech[16] = 2000;
    brech[16] = 1.0002731583221;
    wavelength_brech[17] = 2200;
    brech[17] = 1.0002731583221;
    wavelength_brech[18] = 2400;
    brech[18] = 1.0002731583221;
    wavelength_brech[19] = 2500;
    brech[19] = 1.0002731583221;

    // absorb
    wavelength_absorb[0] = 300;
    absorb[0] = 0;
    wavelength_absorb[1] = 305;
    absorb[1] = 0;
    wavelength_absorb[2] = 315;
    absorb[2] = 0;
    wavelength_absorb[3] = 325;
    absorb[3] = 0;
    wavelength_absorb[4] = 330;
    absorb[4] = 0;
    wavelength_absorb[5] = 400;
    absorb[5] = 0;
    wavelength_absorb[6] = 500;
    absorb[6] = 0;
    wavelength_absorb[7] = 600;
    absorb[7] = 0;
    wavelength_absorb[8] = 700;
    absorb[8] = 0;
    wavelength_absorb[9] = 800;
    absorb[9] = 0;
    wavelength_absorb[10] = 900;
    absorb[10] = 0;
    wavelength_absorb[11] = 1000;
    absorb[11] = 0;
    wavelength_absorb[12] = 1200;
    absorb[12] = 0;
    wavelength_absorb[13] = 1400;
    absorb[13] = 0;
    wavelength_absorb[14] = 1600;
    absorb[14] = 0;
    wavelength_absorb[15] = 1800;
    absorb[15] = 0;
    wavelength_absorb[16] = 2000;
    absorb[16] = 0;
    wavelength_absorb[17] = 2200;
    absorb[17] = 0;
    wavelength_absorb[18] = 2400;
    absorb[18] = 0;
    wavelength_absorb[19] = 2500;
    absorb[19] = 0;

    // build Spline_Interpolation
    tk::spline brech_spline;
    brech_spline.set_points(wavelength_brech, brech);
    tk::spline absorb_spline;
    absorb_spline.set_points(wavelength_absorb, absorb);

    interpol splin_int;
    splin_int.absorb_spline = absorb_spline;
    splin_int.brech_spline = brech_spline;

    return splin_int;
}






interpol interpol_BK7() {
    std::vector<double> brech(39,0);
    std::vector<double> wavelength_brech(39,0);
    std::vector<double> absorb(13,0);
    std::vector<double> wavelength_absorb(13,0);

    // Hard_Code Values
    // Brechzahl
    wavelength_brech[0] =300;
    brech[0] = 1.5527702635739;
    wavelength_brech[1] =322;
    brech[1] = 1.5458699289209;
    wavelength_brech[2] =344;
    brech[2] = 1.5404466868331;
    wavelength_brech[3] =366;
    brech[3] = 1.536090527917;
    wavelength_brech[4] =388;
    brech[4] = 1.53252773217;
    wavelength_brech[5] =410;
    brech[5] = 1.529568767224;
    wavelength_brech[6] =432;
    brech[6] = 1.5270784291406;
    wavelength_brech[7] =454;
    brech[7] = 1.5249578457324;
    wavelength_brech[8] =476;
    brech[8] = 1.5231331738499;
    wavelength_brech[9] =498;
    brech[9] = 1.5215482528369;
    wavelength_brech[10] =520;
    brech[10] = 1.5201596882463;
    wavelength_brech[11] =542;
    brech[11] = 1.5189334783109;
    wavelength_brech[12] =564;
    brech[12] = 1.5178426478869;
    wavelength_brech[13] = 586;
    brech[13] = 1.516865556749;
    wavelength_brech[14] = 608;
    brech[14] = 1.5159846691816;
    wavelength_brech[15] = 630;
    brech[15] = 1.5151856452759;
    wavelength_brech[16] = 674;
    brech[16] =  1.513787889767;
    wavelength_brech[17] = 696;
    brech[17] = 1.5131711117948;
    wavelength_brech[18] = 740;
    brech[18] = 1.5120668948646;
    wavelength_brech[19] = 784;
    brech[19] =   1.5111002059336;
    wavelength_brech[20] = 828;
    brech[20] = 1.5102389559626;
    wavelength_brech[21] = 894;
    brech[21] = 1.5090939781792;
    wavelength_brech[22] = 938;
    brech[22] = 1.5084036266923;
    wavelength_brech[23] = 1048;
    brech[23] = 1.5068468735887;
    wavelength_brech[24] = 1136;
    brech[24] = 1.5057100370628;
    wavelength_brech[25] = 1246;
    brech[25] = 1.5043560652815;
    wavelength_brech[26] = 1356;
    brech[26] = 1.5030283541053;
    wavelength_brech[27] = 1444;
    brech[27] = 1.5019615474168;
    wavelength_brech[28] = 1554;
    brech[28] = 1.5006018978322;
    wavelength_brech[29] = 1664;
    brech[29] = 1.4991968790539;
    wavelength_brech[30] = 1752;
    brech[30] = 1.4980312362468;
    wavelength_brech[31] = 1862;
    brech[31] = 1.4965134002084;
    wavelength_brech[32] = 1950;
    brech[32] = 1.4952451698788;
    wavelength_brech[33] = 2060;
    brech[33] = 1.4935863103651;
    wavelength_brech[34] = 2148;
    brech[34] = 1.4921963626087;
    wavelength_brech[35] = 2258;
    brech[35] = 1.4903754502768;
    wavelength_brech[36] = 2346;
    brech[36] = 1.4888485314127;
    wavelength_brech[37] = 2456;
    brech[37] = 1.4868477447166;
    wavelength_brech[38] = 2500;
    brech[38] = 1.486017709571;

    // Absorbtion
    wavelength_absorb[0] = 300;
    absorb[0] = 2.8607E-06;
    wavelength_absorb[1] = 310;
    absorb[1] = 1.3679E-06;
    wavelength_absorb[2] = 320;
    absorb[2] = 6.6608E-07;
    wavelength_absorb[3] = 350;
    absorb[3] = 9.2894E-08;
    wavelength_absorb[4] = 436;
    absorb[4] = 1.1147E-08;
    wavelength_absorb[5] = 546;
    absorb[5] = 6.9658E-09;
    wavelength_absorb[6] = 660;
    absorb[6] = 1.2643E-08;
    wavelength_absorb[7] = 700;
    absorb[7] = 8.9305E-09;
    wavelength_absorb[8] = 1060;
    absorb[8] = 1.0137E-08;
    wavelength_absorb[9] = 1530;
    absorb[9] = 9.8390E-08;
    wavelength_absorb[10] = 1970;
    absorb[10] = 1.0933E-06;
    wavelength_absorb[11] = 2325;
    absorb[11] = 4.2911E-06;
    wavelength_absorb[12] = 2500;
    absorb[12] = 8.1300E-06;

    // build Spline_Interpolation
    tk::spline brech_spline;
    brech_spline.set_points(wavelength_brech, brech);
    tk::spline absorb_spline;
    absorb_spline.set_points(wavelength_absorb, absorb);

    interpol splin_int;
    splin_int.absorb_spline = absorb_spline;
    splin_int.brech_spline = brech_spline;

    return splin_int;
}


interpol interpol_SiO2() {


    std::vector<double> brech(28,0);
    std::vector<double> wavelength_brech(28,0);

    std::vector<double> absorb(20,0);
    std::vector<double> wavelength_absorb(20,0);

    // Hard_Code Values
    // Brechzahl
    wavelength_brech[0] = 300;
    brech[0] = 1.500288;
    wavelength_brech[1] = 305;
    brech[1] = 1.499055;
    wavelength_brech[2] = 310;
    brech[2] = 1.497885;
    wavelength_brech[3] = 335;
    brech[3] = 1.492849;
    wavelength_brech[4] = 360;
    brech[4] = 1.488882;
    wavelength_brech[5] = 400;
    brech[5] = 1.484080;
    wavelength_brech[6] = 475;
    brech[6] = 1.478225;
    wavelength_brech[7] = 575;
    brech[7] = 1.473766;
    wavelength_brech[8] = 675;
    brech[8] = 1.471166;
    wavelength_brech[9] = 775;
    brech[9] = 1.469517;
    wavelength_brech[10] = 875;
    brech[10] = 1.468405;
    wavelength_brech[11] = 975;
    brech[11] = 1.467620;
    wavelength_brech[12] = 1075;
    brech[12] = 1.467045;
    wavelength_brech[13] = 1175;
    brech[13] = 1.466611;
    wavelength_brech[14] = 1275;
    brech[14] = 1.466276;
    wavelength_brech[15] = 1375;
    brech[15] = 1.466011;
    wavelength_brech[16] = 1475;
    brech[16] = 1.465798;
    wavelength_brech[17] = 1575;
    brech[17] = 1.465625;
    wavelength_brech[18] = 1675;
    brech[18] = 1.465482;
    wavelength_brech[19] = 1775;
    brech[19] = 1.465362;
    wavelength_brech[20] = 1875;
    brech[20] = 1.465261;
    wavelength_brech[21] = 1975;
    brech[21] = 1.465175;
    wavelength_brech[22] = 2075;
    brech[22] = 1.465102;
    wavelength_brech[23] = 2175;
    brech[23] = 1.465038;
    wavelength_brech[24] = 2275;
    brech[24] = 1.464982;
    wavelength_brech[25] = 2375;
    brech[25] = 1.464933;
    wavelength_brech[26] = 2475;
    brech[26] = 1.464890;
    wavelength_brech[27] = 2500;
    brech[27] = 1.464881;


    // absorb
    wavelength_absorb[0] = 300;
    absorb[0] = 0.000132;
    wavelength_absorb[1] = 305;
    absorb[1] = 0.000125;
    wavelength_absorb[2] = 315;
    absorb[2] = 0.000112;
    wavelength_absorb[3] = 325;
    absorb[3] = 0.000102;
    wavelength_absorb[4] = 330;
    absorb[4] = 0;
    wavelength_absorb[5] = 400;
    absorb[5] = 0;
    wavelength_absorb[6] = 500;
    absorb[6] = 0;
    wavelength_absorb[7] = 600;
    absorb[7] = 0;
    wavelength_absorb[8] = 700;
    absorb[8] = 0;
    wavelength_absorb[9] = 800;
    absorb[9] = 0;
    wavelength_absorb[10] = 900;
    absorb[10] = 0;
    wavelength_absorb[11] = 1000;
    absorb[11] = 0;
    wavelength_absorb[12] = 1200;
    absorb[12] = 0;
    wavelength_absorb[13] = 1400;
    absorb[13] = 0;
    wavelength_absorb[14] = 1600;
    absorb[14] = 0;
    wavelength_absorb[15] = 1800;
    absorb[15] = 0;
    wavelength_absorb[16] = 2000;
    absorb[16] = 0;
    wavelength_absorb[17] = 2200;
    absorb[17] = 0;
    wavelength_absorb[18] = 2400;
    absorb[18] = 0;
    wavelength_absorb[19] = 2500;
    absorb[19] = 0;

    // build Spline_Interpolation
    tk::spline brech_spline;
    brech_spline.set_points(wavelength_brech, brech);
    tk::spline absorb_spline;
    absorb_spline.set_points(wavelength_absorb, absorb);
    interpol splin_int;
    splin_int.absorb_spline = absorb_spline;
    splin_int.brech_spline = brech_spline;

    return splin_int;
}



interpol interpol_TiO2() {


    std::vector<double> brech(23,0);
    std::vector<double> wavelength_brech(23,0);

    std::vector<double> absorb(20,0);
    std::vector<double> wavelength_absorb(20,0);

    // Hard_Code Values
    // Brechzahl
    wavelength_brech[0] = 300;
    brech[0] = 2.809982;
    wavelength_brech[1] = 310;
    brech[1] = 2.813419;
    wavelength_brech[2] = 320;
    brech[2] = 2.816447;
    wavelength_brech[3] = 350;
    brech[3] = 2.585271;
    wavelength_brech[4] = 450;
    brech[4] = 2.243962;
    wavelength_brech[5] = 550;
    brech[5] = 2.164358;
    wavelength_brech[6] = 650;
    brech[6] = 2.125097;
    wavelength_brech[7] = 750;
    brech[7] = 2.102809;
    wavelength_brech[8] = 850;
    brech[8] = 2.088766;
    wavelength_brech[9] = 950;
    brech[9] = 2.079288;
    wavelength_brech[10] = 1050;
    brech[10] = 2.072565;
    wavelength_brech[11] = 1150;
    brech[11] = 2.067613;
    wavelength_brech[12] = 1250;
    brech[12] = 2.063855;
    wavelength_brech[13] = 1350;
    brech[13] = 2.060932;
    wavelength_brech[14] = 1450;
    brech[14] = 2.058612;
    wavelength_brech[15] = 1550;
    brech[15] = 2.056740;
    wavelength_brech[16] = 1650;
    brech[16] = 2.055136;
    wavelength_brech[17] = 1690;
    brech[17] = 2.054669;
    wavelength_brech[18] = 1750;
    brech[18] = 2.054669;
    wavelength_brech[19] = 1850;
    brech[19] = 2.054669;
    wavelength_brech[20] = 1950;
    brech[20] = 2.054669;
    wavelength_brech[21] = 2250;
    brech[21] = 2.054669;
    wavelength_brech[22] = 2500;
    brech[22] = 2.054669;


    // absorb
    wavelength_absorb[0] = 300;
    absorb[0] = 0;
    wavelength_absorb[1] = 305;
    absorb[1] = 0;
    wavelength_absorb[2] = 315;
    absorb[2] = 0;
    wavelength_absorb[3] = 325;
    absorb[3] = 0;
    wavelength_absorb[4] = 330;
    absorb[4] = 0;
    wavelength_absorb[5] = 400;
    absorb[5] = 0;
    wavelength_absorb[6] = 500;
    absorb[6] = 0;
    wavelength_absorb[7] = 600;
    absorb[7] = 0;
    wavelength_absorb[8] = 700;
    absorb[8] = 0;
    wavelength_absorb[9] = 800;
    absorb[9] = 0;
    wavelength_absorb[10] = 900;
    absorb[10] = 0;
    wavelength_absorb[11] = 1000;
    absorb[11] = 0;
    wavelength_absorb[12] = 1200;
    absorb[12] = 0;
    wavelength_absorb[13] = 1400;
    absorb[13] = 0;
    wavelength_absorb[14] = 1600;
    absorb[14] = 0;
    wavelength_absorb[15] = 1800;
    absorb[15] = 0;
    wavelength_absorb[16] = 2000;
    absorb[16] = 0;
    wavelength_absorb[17] = 2200;
    absorb[17] = 0;
    wavelength_absorb[18] = 2400;
    absorb[18] = 0;
    wavelength_absorb[19] = 2500;
    absorb[19] = 0;

    // build Spline_Interpolation
    tk::spline brech_spline;
    brech_spline.set_points(wavelength_brech, brech);
    tk::spline absorb_spline;
    absorb_spline.set_points(wavelength_absorb, absorb);
    interpol splin_int;
    splin_int.absorb_spline = absorb_spline;
    splin_int.brech_spline = brech_spline;

    return splin_int;
}



interpol interpol_Ta2O5() {


    std::vector<double> brech(50,0);
    std::vector<double> wavelength_brech(50,0);

    std::vector<double> absorb(47,0);
    std::vector<double> wavelength_absorb(47,0);

    // Hard_Code Values
    // Brechzahl
    wavelength_brech[0] = 30.0;
    brech[0] = 0.883526;
    wavelength_brech[1] = 35.5;
    brech[1] = 0.859036;
    wavelength_brech[2] = 40.5;
    brech[2] = 0.872251;
    wavelength_brech[3] = 44.47;
    brech[3] = 0.880129;
    wavelength_brech[4] = 49.74;
    brech[4] = 0.878349;
    wavelength_brech[5] = 55.6;
    brech[5] = 0.909900;
    wavelength_brech[6] = 61.0;
    brech[6] = 0.954689;
    wavelength_brech[7] = 65.8;
    brech[7] = 0.979227;
    wavelength_brech[8] = 70.9;
    brech[8] = 1.018160;
    wavelength_brech[9] = 75.0;
    brech[9] = 1.058245;
    wavelength_brech[10] = 80.8;
    brech[10] = 1.113578;
    wavelength_brech[11] = 90.4;
    brech[11] = 1.222391;
    wavelength_brech[12] =101 ;
    brech[12] = 1.462605;
    wavelength_brech[13] = 149.6;
    brech[13] = 1.803949;
    wavelength_brech[14] = 201.7;
    brech[14] = 2.124363;
    wavelength_brech[15] = 247.6;
    brech[15] = 2.611533;
    wavelength_brech[16] = 298.5;
    brech[16] = 2.377475;
    wavelength_brech[17] = 346.57;
    brech[17] = 2.262346;
    wavelength_brech[18] = 402.38;
    brech[18] = 2.196309;
    wavelength_brech[19] = 450;
    brech[19] = 2.160999;
    wavelength_brech[20] = 503.41;
    brech[20] = 2.133353;
    wavelength_brech[21] = 552.65;
    brech[21] = 2.114865;
    wavelength_brech[22] = 606.72;
    brech[22] = 2.100052;
    wavelength_brech[23] = 653.7;
    brech[23] = 2.090601;
    wavelength_brech[24] = 704.4;
    brech[24] = 2.082859;
    wavelength_brech[25] = 759;
    brech[25] = 2.076014;
    wavelength_brech[26] = 802.7;
    brech[26] = 2.071678;
    wavelength_brech[27] = 849;
    brech[27] = 2.067873;
    wavelength_brech[28] = 897.89;
    brech[28] = 2.064523;
    wavelength_brech[29] = 949.6;
    brech[29] = 2.061544;
    wavelength_brech[30] = 1004.3;
    brech[30] = 2.058858;
    wavelength_brech[31] = 1042.5;
    brech[31] = 2.057206;
    wavelength_brech[32] = 1102.5;
    brech[32] = 2.054984;
    wavelength_brech[33] = 1144.4;
    brech[33] = 2.053640;
    wavelength_brech[34] = 1210.4;
    brech[34] = 2.051790;
    wavelength_brech[35] = 1256.4;
    brech[35] = 2.050644;
    wavelength_brech[36] = 1304.2;
    brech[36] = 2.049564;
    wavelength_brech[37] = 1353.8;
    brech[37] = 2.048546;
    wavelength_brech[38] = 1405.3;
    brech[38] = 2.047633;
    wavelength_brech[39] = 1468.8;
    brech[39] = 2.046804;
    wavelength_brech[40] = 1514.3;
    brech[40] = 2.046043;
    wavelength_brech[41] = 1600;
    brech[41] = 2.046043;
    wavelength_brech[42] = 1800;
    brech[42] = 2.046043;
    wavelength_brech[43] = 1900;
    brech[43] = 2.046043;
    wavelength_brech[44] = 2000;
    brech[44] = 2.046043;
    wavelength_brech[45] = 2100;
    brech[45] = 2.046043;
    wavelength_brech[46] = 2200;
    brech[46] = 2.046043;
    wavelength_brech[47] = 2300;
    brech[47] = 2.046043;
    wavelength_brech[48] = 2400;
    brech[48] = 2.046043;
    wavelength_brech[49] = 2500;
    brech[49] = 2.046043;

    // absorb
    wavelength_absorb[0] = 30.0;
    absorb[0] = 0.188000;
    wavelength_absorb[1] = 35.5;
    absorb[1] = 0.273000;
    wavelength_absorb[2] = 40.5;
    absorb[2] = 0.339000;
    wavelength_absorb[3] = 45.3;
    absorb[3] = 0.391000;
    wavelength_absorb[4] = 50.6;
    absorb[4] = 0.461000;
    wavelength_absorb[5] = 59.9;
    absorb[5] = 0.609000;
    wavelength_absorb[6] = 70.9;
    absorb[6] = 0.728000;
    wavelength_absorb[7] = 80.8;
    absorb[7] = 0.824000;
    wavelength_absorb[8] = 90.4;
    absorb[8] = 0.943001;
    wavelength_absorb[9] = 101.1;
    absorb[9] = 1.050000;
    wavelength_absorb[10] = 119.5;
    absorb[10] = 0.880978;
    wavelength_absorb[11] = 149.6;
    absorb[11] = 0.753066;
    wavelength_absorb[12] = 201.7;
    absorb[12] = 0.987470;
    wavelength_absorb[13] = 252.4;
    absorb[13] = 0.387131;
    wavelength_absorb[14] = 304.1;
    absorb[14] = 0.112057;
    wavelength_absorb[15] = 353.1;
    absorb[15] = 0.059838;
    wavelength_absorb[16] = 402.4;
    absorb[16] = 0.037077;
    wavelength_absorb[17] = 450.0;
    absorb[17] = 0.023992;
    wavelength_absorb[18] = 503.4;
    absorb[18] = 0.015261;
    wavelength_absorb[19] = 552.6;
    absorb[19] = 0.010744;
    wavelength_absorb[20] = 606.7;
    absorb[20] = 0.007976;
    wavelength_absorb[21] = 653.7;
    absorb[21] = 0.006599;
    wavelength_absorb[22] = 704.4;
    absorb[22] = 0.005339;
    wavelength_absorb[23] = 759.0;
    absorb[23] = 0.004288;
    wavelength_absorb[24] = 802.76;
    absorb[24] = 0.003752;
    wavelength_absorb[25] = 848.9;
    absorb[25] = 0.003264;
    wavelength_absorb[26] = 897.9;
    absorb[26] = 0.002819;
    wavelength_absorb[27] = 949.6;
    absorb[27] = 0.002397;
    wavelength_absorb[28] = 1004.3;
    absorb[28] = 0.001993;
    wavelength_absorb[29] = 1042.5;
    absorb[29] = 0.001791;
    wavelength_absorb[30] = 1102.5;
    absorb[30] = 0.001527;
    wavelength_absorb[31] = 1166.0;
    absorb[31] = 0.001277;
    wavelength_absorb[32] = 1201.4;
    absorb[32] = 0.001118;
    wavelength_absorb[33] = 1256.4;
    absorb[33] = 0.000974;
    wavelength_absorb[34] = 1304.2;
    absorb[34] = 0.000852;
    wavelength_absorb[35] = 1353.8;
    absorb[35] = 0.000790;
    wavelength_absorb[36] = 1405.3;
    absorb[36] = 0.000757;
    wavelength_absorb[37] = 1458.8;
    absorb[37] = 0.000725;
    wavelength_absorb[38] = 1514.3;
    absorb[38] = 0.000694;
    wavelength_absorb[39] = 1600;
    absorb[39] = 0.000694;
    wavelength_absorb[40] = 1700;
    absorb[40] = 0.000694;
    wavelength_absorb[41] = 1800;
    absorb[41] = 0.000694;
    wavelength_absorb[42] = 2000;
    absorb[42] = 0.000694;
    wavelength_absorb[43] = 2200;
    absorb[43] = 0.000694;
    wavelength_absorb[44] = 2300;
    absorb[44] = 0.000694;
    wavelength_absorb[45] = 2400;
    absorb[45] = 0.000694;
    wavelength_absorb[46] = 2500;
    absorb[46] = 0.000694;

    // build Spline_Interpolation
    tk::spline brech_spline;
    brech_spline.set_points(wavelength_brech, brech);
    tk::spline absorb_spline;
    absorb_spline.set_points(wavelength_absorb, absorb);
    interpol splin_int;
    splin_int.absorb_spline = absorb_spline;
    splin_int.brech_spline = brech_spline;

    return splin_int;
}

#endif /* values_SPLINE_H */