# optical thin film calculator

## 1.  Project description

This is an C++ implementation of an [optical thin film](https://en.wikipedia.org/wiki/Thin-film_optics) calculator. It is a brute-force algorithm that tries to find the right combination of material thickness for a given optical thin film system. This work started as a university poject for algorithm engineering. The aim of the project is to utilize modern hardware efficiently. The algorithm makes use of:

1. [parallel computing](https://en.wikipedia.org/wiki/Parallel_computing)
1. [automatic vectorization](https://en.wikipedia.org/wiki/Automatic_vectorization)
1. [cache blocking](https://software.intel.com/en-us/articles/cache-blocking-techniques)


Until now it only supports the computation of reflectivity values for incidence of light of 0 degree. The project includes a cython implementation and a jupyter notebook, which explains Basic Usage in Python (see . Basic usage in python). The implementation uses the  [notation](https://www.iap.uni-jena.de/iapmedia/de/Lecture/Grundkonzepte+der+Optik1414710000/GdO_SS2014_Optikskript_Lederer_1999WS.pdf) from Professor Falk Lederer institut for Solid-state theory and theoretical optics Friedrich-Schiller-University Jena. Refractive values ​​from [refractiveindex.info](https://refractiveindex.info/)  were used for testing.



## 2. Dependencies
OpenMP, Cmake minimum VERSION 3.7.2 and C++14 is required to compile the project.


## 3. Core ideas

For detailed information see presentation.pdf.


## 4. Input Data-structures for thin_film_calculator

 thin_film_calculator searches for a suitable combination of layer thicknesses for a given layer system, so that the reflectivity values ​​desired in vec_targets are met.


```cpp

void thin_film_calculator  (double *  __restrict__ wavelength,
                            double *  __restrict__ vec_brechzahl,
                            double *  __restrict__ vec_absorb,
                            double *  __restrict__ dn_thickness,
                            double *  __restrict__ vec_targets,
                            double *  __restrict__ final_solution,
                            int num_stuetz ,
                            int num_schicht,
                            int num_dick,
                            int num_mat,
                            int zwl,
                            uint64_t max_num_iterations,
                            int size_L1_cache_in_KB = 256);

```



| Parameter                             | Description                                                      			|
|:--------------------------------------|:--------------------------------------------------------------------------|
| wavelength 	  						| wavelengths at which the reflectivity should be calculated 				|
| vec_brechzahl    						| vector of the refractive index of the materials used for each wavelength  |
| vec_absorb		  					| vector of the absorption of the materials used for each wavelength        |
| vec_targets  	  						| target values ​​of the desired reflectivity at the wavelengths              |
| final_solution   						| output of the function, (preallocated memory)								|
| num_stuetz               			  	| number of wavelengths. corresponds to the length of the vector wavelength |
| num_schicht        				  	| number of layers in the layer system + 2 for air and glas                 |
| num_dick                            	| number of discretization steps for the thickness between [0 , 3* zwl/ 4]  |
| num_mat        					  	| number of coating materials in the thin film system                  		|
| zwl                                 	| central wavelength, roughly corresponds to the median of wavelength   	|
| max_num_iterations			  		| maximum number of iterations the search should run 						|
| size_L1_cache_in_KB = 256			  	| size of L1_cache on target machine, default is 256		 				|



Important: 
Wavelength is sorted increasing from lower to larger wavelengths. 
vec_breachzahl and vec_absorb should contain the specific values for every wavelength for every material, sorted in the order of incident ligth (see 5. Basic Usage in C++ example). 







## 5. Basic Usage in C++ example


```cpp

#include <vector>
#include <iostream>
#include "thin_film_calc.h"
#include "aligned_vector.h"     // for vector alignment


using namespace std;

template <class T>
using aligned_vector = std::vector<T, alligned_allocator<T, 64>>;  // use 64 bit aligned vectors


int main() {
    // create realistic test case
    const double zwl = 1030.0;
    const int num_stuetz = 21;
    const int num_schicht = 6;
    const int num_mat = 2;
    aligned_vector<double> wavelength = linespace_wavelength(num_stuetz, 1020, 1041);
    aligned_vector<double> vec_targets (num_stuetz,0.01);  // 0,01 is 1 percent reflectivity,if set to 0.0 no solution is found and all max_num_iterations are computed (for testing)
    uint64_t max_num_iterations = 10000;
    aligned_vector<double> vec_omega (num_stuetz);
    aligned_vector<double> vec_brechzahl ((num_mat+2) * num_stuetz);
    aligned_vector<double> vec_absorb ((num_mat+2) * num_stuetz);
    aligned_vector<double> final_solution (num_schicht,0);


    // choose discretization of the search space 
    //int num_dick = 12;     // = 230 Kbytes data
    //int num_dick = 50;       // = 960 kbytes data
    int num_dick = 300;    // = 5760 kbytes data  // --> would be the most common use case
    //int num_dick = 3000;   //  57600 Kilobytes 

    // discretize thickness , is  equivalent to discretization of the search space
    aligned_vector <double> dn_thickness(num_dick,0);
    double steps = (3* (zwl/4)) / num_dick;  // this step size has physical reasons
    for (int i = 0 ; i < num_dick ; i++){
        dn_thickness[i] = (i+1) * steps;
    }

    // build input data
    build_data (wavelength.data(), vec_brechzahl.data(), vec_absorb.data(), num_stuetz, num_mat  );

    thin_film_calculator  (wavelength.data(),    vec_brechzahl.data(), vec_absorb.data(), dn_thickness.data(), vec_targets.data(), final_solution.data(), \
    num_stuetz ,num_schicht, num_dick, num_mat, zwl, max_num_iterations);

    cout << "Solution thickness is:" << endl;
    for (auto iv : final_solution){
        cout << iv << endl;
    }

}
    
```



## 6. Basic usage in python

The folder pyton contains a cython version of the C++ implemenation and a jupyter notebook jupyter_notebook_example.ipynb which explains basic usage in python.



