#define CATCH_CONFIG_MAIN
#include <catch.h>
#include <type_traits>
#include "thin_film_calc.h"
#include <complex>
#include <vector>

// Defines test scenarios for thin_film_calc.cpp
// used in gitlab CI

using namespace Catch::Matchers;


TEST_CASE("matrix_product", "[first]") {
    std::vector <std::complex<double>> A(4);
    std::vector <std::complex<double>> B(4);
    A[0] = {1,0};
    A[1] = {0,1};
    A[2] = {0,1};
    A[3] = {1,0};
    B[0] = {2,0};
    B[1] = {1,2};
    B[2] = {-2,0};
    B[3] = {4,-2};
    matrix_product(A.data(), B.data());
    REQUIRE(A[0] == std::complex<double>{2,-2});
    REQUIRE(A[1] == std::complex<double>{3,6});
    REQUIRE(A[2] == std::complex<double>{-2,2});
    REQUIRE(A[3] == std::complex<double>{2,-1});
}

TEST_CASE("calc_interim", "[second]") {
    int wl_test =  550;
    int num_mat =    1;
    int num_stuetz = 1;
    int num_dick =   1;
    aligned_vector<std::complex<double>> k_ix_omega_c0(num_mat * num_stuetz);
    double omega_c0_test = (2.0 * M_PI) / (wl_test);
    std::complex<double> temp = std::complex<double>{1.38, 0} * omega_c0_test;
    k_ix_omega_c0[0] = temp;  // Material * omega_c0
    std::vector<double> dn_thickness(num_dick  , wl_test / (1.38 * 4) );
    std::vector<std::complex<double>> precomp_matricies(num_stuetz * num_dick * num_mat * 4);  // = 4 entries
    calc_interims(k_ix_omega_c0.data() , dn_thickness.data(), num_mat, num_stuetz,   num_dick,  precomp_matricies.data());
    REQUIRE(precomp_matricies[0].real() <  0.0001);
    REQUIRE(precomp_matricies[0].real() >  -0.00001);
    REQUIRE(precomp_matricies[0].imag() == 0);
    REQUIRE(precomp_matricies[1].real() > 63.4313);
    REQUIRE(precomp_matricies[1].real() < 63.4314);
    REQUIRE(precomp_matricies[1].imag() == 0);
    REQUIRE(precomp_matricies[2].real() < -0.0157650);
    REQUIRE(precomp_matricies[2].real() > -0.0157652);
    REQUIRE(precomp_matricies[2].imag() == 0);
    REQUIRE(precomp_matricies[3].real() < 0.0001 );
    REQUIRE(precomp_matricies[3].real() >  -0.00001);
    REQUIRE(precomp_matricies[3].imag() == 0);
}


TEST_CASE("calc_TransferMatrix_precomp", "[third]") {
    int wl_test = 550;
    int num_mat = 1;
    int num_stuetz = 1;
    int num_dick = 1;
    int num_schicht = 3;
    aligned_vector<std::complex<double>> k_ix_omega_c0(num_mat * num_stuetz);
    double omega_c0_test = (2.0 * M_PI) / (wl_test);
    std::complex<double> temp = std::complex<double>{1.38, 0} * omega_c0_test;
    k_ix_omega_c0[0] = temp;  // Material * omega_c0
    std::vector<double> dn_thickness(num_dick, wl_test / (1.38 * 4));
    std::vector<std::complex<double>> precomp_matricies(num_stuetz * num_dick * num_mat * 4);  // = 4 entries
    calc_interims(k_ix_omega_c0.data(), dn_thickness.data(), num_mat, num_stuetz, num_dick, precomp_matricies.data());
    aligned_vector<std::complex<double>> tMatrix{std::complex<double>(1, 0), std::complex<double>(0, 0),
                                                 std::complex<double>(0, 0), std::complex<double>(1, 0)};
    aligned_vector<int> rand_dick(num_schicht, 0);
    int stuetz_id = 0;
    calc_TransferMatrix_precomp(precomp_matricies.data(), tMatrix.data(), rand_dick.data(), num_mat, num_schicht,
                                num_dick, stuetz_id);
    REQUIRE(tMatrix[0].real() <  0.0001);
    REQUIRE(tMatrix[0].real() >  -0.00001);
    REQUIRE(tMatrix[0].imag() == 0);
    REQUIRE(tMatrix[1].real() > 63.4313);
    REQUIRE(tMatrix[1].real() < 63.4314);
    REQUIRE(tMatrix[1].imag() == 0);
    REQUIRE(tMatrix[2].real() < -0.0157650);
    REQUIRE(tMatrix[2].real() > -0.0157652);
    REQUIRE(tMatrix[2].imag() == 0);
    REQUIRE(tMatrix[3].real() < 0.0001 );
    REQUIRE(tMatrix[3].real() >  -0.00001);
    REQUIRE(tMatrix[3].imag() == 0);
}




TEST_CASE("calc_RTP_1", "[4th]") {
    /// one layer lambda/4
    std::complex<double> imag_unit{0,1};
    int num_schicht_test = 3;
    double wl_test = 550;
    double omega_c0_test = (2.0 * M_PI) / (wl_test);
    aligned_vector<double> dn_thick_test{100000, wl_test / (1.38 * 4), 10000};
    aligned_vector<std::complex<double>> dn_k_ix(num_schicht_test);
    dn_k_ix[0] = {1, 0};     // Air
    dn_k_ix[1] = {1.38, 0};  // Material
    dn_k_ix[2] = {1.51, 0};  // Glas
    aligned_vector<std::complex<double>> k_ix_omega_c0(num_schicht_test);
    aligned_vector<std::complex<double>> interim(4);
    aligned_vector<std::complex<double>> M{std::complex<double>(1, 0), std::complex<double>(0, 0), std::complex<double>(0, 0), std::complex<double>(1, 0)};
    calc_k_ix_omega_c0(dn_k_ix.data(),  num_schicht_test, omega_c0_test,  k_ix_omega_c0.data());
    calc_TransferMatrix(k_ix_omega_c0.data(), dn_thick_test.data(), M.data(), interim.data(), num_schicht_test);

    auto k_inx = k_ix_omega_c0.back().real();
    auto k_outx = k_ix_omega_c0[0].real();
    double result = calc_RTP(M.data(), k_outx, k_inx);
    REQUIRE(result > 0.0133);
    REQUIRE(result < 0.0134);
    }


TEST_CASE("calc_RTP_2", "[5th]") {
    /// three layers lambda/4
    int num_schicht_test = 5;
    double wl_test = 550;
    double omega_c0_test = (2.0 * M_PI) / (wl_test);
    aligned_vector<double> dn_thick_test{100000, wl_test / (1.38 * 4)  ,  wl_test / (2.0 * 4) , wl_test / (1.8 * 4)  ,  10000 };
    aligned_vector<std::complex<double>> dn_k_ix_2(num_schicht_test);
    dn_k_ix_2[0] = {1, 0};      // Air
    dn_k_ix_2[1] = {1.38, 0};   // Material 1
    dn_k_ix_2[2] = {2.0, 0};    // Material 2
    dn_k_ix_2[3] = {1.8, 0};    // Material 3
    dn_k_ix_2[4] = {1.51, 0};   // Glas
    aligned_vector<std::complex<double>> k_ix_omega_c0_2(num_schicht_test);
    aligned_vector<std::complex<double>> interim(4);
    aligned_vector<std::complex<double>> M{std::complex<double>(1, 0), std::complex<double>(0, 0), std::complex<double>(0, 0), std::complex<double>(1, 0)};
    calc_k_ix_omega_c0(dn_k_ix_2.data(), num_schicht_test,omega_c0_test, k_ix_omega_c0_2.data());
    calc_TransferMatrix(k_ix_omega_c0_2.data(), dn_thick_test.data(), M.data(), interim.data(), num_schicht_test);
    auto k_inx = k_ix_omega_c0_2.back().real();
    auto k_outx = k_ix_omega_c0_2[0].real();
    double result = calc_RTP(M.data(), k_outx, k_inx);
    REQUIRE(result > 0.0001138);
    REQUIRE(result < 0.0001139);
}


TEST_CASE("calc_RTP_3", "[6th]") {
    /// three layers lambda/4  lambda/2  lambda/4
    std::complex<double> imag_unit{0,1};
    int num_schicht_test = 5;
    double wl_test = 550;
    double omega_c0_test = (2.0 * M_PI) / (wl_test);
    aligned_vector<double> dn_thick_test{100000, wl_test / (1.38 * 4)  ,  wl_test / (2.2 * 2) , wl_test / (1.7 * 4)  ,  10000};
    aligned_vector<std::complex<double>> dn_k_ix_2(num_schicht_test);
    dn_k_ix_2[0] = {1, 0};
    dn_k_ix_2[1] = {1.38, 0};
    dn_k_ix_2[2] = {2.2, 0};
    dn_k_ix_2[3] = {1.7, 0};
    dn_k_ix_2[4] = {1.51, 0};
    aligned_vector<std::complex<double>> k_ix_omega_c0_2(num_schicht_test);
    aligned_vector<std::complex<double>> interim(4);
    aligned_vector<std::complex<double>> M{std::complex<double>(1, 0), std::complex<double>(0, 0), std::complex<double>(0, 0), std::complex<double>(1, 0)};
    calc_k_ix_omega_c0(dn_k_ix_2.data(), num_schicht_test, omega_c0_test, k_ix_omega_c0_2.data());
    calc_TransferMatrix(k_ix_omega_c0_2.data(), dn_thick_test.data(), M.data(), interim.data(), num_schicht_test);
    auto k_inx = k_ix_omega_c0_2.back().real();
    auto k_outx = k_ix_omega_c0_2[0].real();
    double result = calc_RTP(M.data(), k_outx, k_inx);
    REQUIRE(result > 0.0000052);
    REQUIRE(result < 0.0000053);
}



TEST_CASE("full integration test 1", "[7th]") {
    const int num_stuetz = 21;
    const int num_schicht = 6;
    int num_dick = 30;
    int num_mat = 2;
    aligned_vector<double> wavelength = linespace_wavelength(num_stuetz, 1020, 1041);
    aligned_vector<double> vec_targets (num_stuetz,0.01);  //   0.01 corresponds to 1 percent reflectivity
    uint64_t max_num_iterations = 100000;
    const double zwl = 1030.0;
    // allocate memory
    aligned_vector<double> vec_omega (num_stuetz);
    aligned_vector<double> vec_brechzahl ((num_mat+2) * num_stuetz);
    aligned_vector<double> vec_absorb ((num_mat+2) * num_stuetz);
    aligned_vector<double> final_solution (num_schicht,0);
    // Generate the input data
    build_data (wavelength.data(), vec_brechzahl.data(), vec_absorb.data(), num_stuetz, num_mat  );
    // Generate vector of discretized thicknesses
    aligned_vector <double> dn_thickness(num_dick,0);
    //initialize possible thicknesses
    double steps = (3* (zwl/4)) / num_dick;
    for (int i = 0 ; i < num_dick ; i++){
        dn_thickness[i] = (i+1) * steps;
    }
    thin_film_calculator  (wavelength.data(),    vec_brechzahl.data(), vec_absorb.data(), dn_thickness.data(), vec_targets.data(), final_solution.data(), \
    num_stuetz ,num_schicht, num_dick, num_mat, zwl, max_num_iterations);
    int num_schicht_test = 6;


    double wl_test = 1020;
    double omega_c0_test = (2.0 * M_PI) / (wl_test);
    aligned_vector<double> dn_thick_test = final_solution;
    aligned_vector<std::complex<double>> dn_k_ix_2(num_schicht_test);
    dn_k_ix_2[0] = {1.00027, 0};
    dn_k_ix_2[1] = {2.05816, 0.00190311};
    dn_k_ix_2[2] = {1.46734, 5.72435e-08};
    dn_k_ix_2[3] = {2.05816, 0.00190311};
    dn_k_ix_2[4] = {1.46734, 5.72435e-08};
    dn_k_ix_2[5] = {1.46734, 5.72435e-08};
    aligned_vector<std::complex<double>> k_ix_omega_c0_2(num_schicht_test);
    aligned_vector<std::complex<double>> interim(4);
    aligned_vector<std::complex<double>> M{std::complex<double>(1, 0), std::complex<double>(0, 0), std::complex<double>(0, 0), std::complex<double>(1, 0)};
    calc_k_ix_omega_c0(dn_k_ix_2.data(), num_schicht_test, omega_c0_test, k_ix_omega_c0_2.data());
    calc_TransferMatrix(k_ix_omega_c0_2.data(), dn_thick_test.data(), M.data(), interim.data(), num_schicht_test);
    auto k_inx = k_ix_omega_c0_2.back().real();
    auto k_outx = k_ix_omega_c0_2[0].real();
    double result = calc_RTP(M.data(), k_outx, k_inx);
    REQUIRE(result < 0.01);

    wl_test = 1040;
    omega_c0_test = (2.0 * M_PI) / (wl_test);
    dn_k_ix_2[0] = {1.00027, 0};
    dn_k_ix_2[1] = {2.05731, 0.00180293};
    dn_k_ix_2[2] = {1.46723, 9.37279e-08};
    dn_k_ix_2[3] = {2.05731, 0.00180293};
    dn_k_ix_2[4] = {1.46723, 9.37279e-08};
    dn_k_ix_2[5] = {1.46723, 9.37279e-08};
    M[0] = 1;
    M[1] = 0;
    M[2] = 0;
    M[3] = 1;
    calc_k_ix_omega_c0(dn_k_ix_2.data(), num_schicht_test, omega_c0_test, k_ix_omega_c0_2.data());
    calc_TransferMatrix(k_ix_omega_c0_2.data(), dn_thick_test.data(), M.data(), interim.data(), num_schicht_test);
    k_inx = k_ix_omega_c0_2.back().real();
    k_outx = k_ix_omega_c0_2[0].real();
    result = calc_RTP(M.data(), k_outx, k_inx);
    REQUIRE(result < 0.01);
}


TEST_CASE("full integration test 2 - Cache fitting", "[8th]") {
    const int num_stuetz = 21;
    const int num_schicht = 6;
    int num_dick = 3000;
    int num_mat = 2;
    aligned_vector<double> wavelength = linespace_wavelength(num_stuetz, 1020, 1041);
    aligned_vector<double> vec_targets (num_stuetz,0.01);
    uint64_t max_num_iterations = 100000;
    const double zwl = 1030.0;
    // allocate memory
    aligned_vector<double> vec_omega (num_stuetz);
    aligned_vector<double> vec_brechzahl ((num_mat+2) * num_stuetz);
    aligned_vector<double> vec_absorb ((num_mat+2) * num_stuetz);
    aligned_vector<double> final_solution (num_schicht,0);

    build_data (wavelength.data(), vec_brechzahl.data(), vec_absorb.data(), num_stuetz, num_mat  );

    aligned_vector <double> dn_thickness(num_dick,0);

    double steps = (3* (zwl/4)) / num_dick;
    for (int i = 0 ; i < num_dick ; i++){
        dn_thickness[i] = (i+1) * steps;
    }
    thin_film_calculator  (wavelength.data(),    vec_brechzahl.data(), vec_absorb.data(), dn_thickness.data(), vec_targets.data(), final_solution.data(), \
    num_stuetz ,num_schicht, num_dick, num_mat, zwl, max_num_iterations);

    int num_schicht_test = 6;
    double wl_test = 1020;
    double omega_c0_test = (2.0 * M_PI) / (wl_test);
    aligned_vector<double> dn_thick_test = final_solution;
    aligned_vector<std::complex<double>> dn_k_ix_2(num_schicht_test);
    dn_k_ix_2[0] = {1.00027, 0};
    dn_k_ix_2[1] = {2.05816, 0.00190311};
    dn_k_ix_2[2] = {1.46734, 5.72435e-08};
    dn_k_ix_2[3] = {2.05816, 0.00190311};
    dn_k_ix_2[4] = {1.46734, 5.72435e-08};
    dn_k_ix_2[5] = {1.46734, 5.72435e-08};
    aligned_vector<std::complex<double>> k_ix_omega_c0_2(num_schicht_test);
    aligned_vector<std::complex<double>> interim(4);
    aligned_vector<std::complex<double>> M{std::complex<double>(1, 0), std::complex<double>(0, 0), std::complex<double>(0, 0), std::complex<double>(1, 0)};
    calc_k_ix_omega_c0(dn_k_ix_2.data(), num_schicht_test, omega_c0_test, k_ix_omega_c0_2.data());
    calc_TransferMatrix(k_ix_omega_c0_2.data(), dn_thick_test.data(), M.data(), interim.data(), num_schicht_test);
    auto k_inx = k_ix_omega_c0_2.back().real();
    auto k_outx = k_ix_omega_c0_2[0].real();
    double result = calc_RTP(M.data(), k_outx, k_inx);
    REQUIRE(result < 0.01);
    wl_test = 1040;
    omega_c0_test = (2.0 * M_PI) / (wl_test);
    dn_k_ix_2[0] = {1.00027, 0};
    dn_k_ix_2[1] = {2.05731, 0.00180293};
    dn_k_ix_2[2] = {1.46723, 9.37279e-08};
    dn_k_ix_2[3] = {2.05731, 0.00180293};
    dn_k_ix_2[4] = {1.46723, 9.37279e-08};
    dn_k_ix_2[5] = {1.46723, 9.37279e-08};
    M[0] = 1;
    M[1] = 0;
    M[2] = 0;
    M[3] = 1;
    calc_k_ix_omega_c0(dn_k_ix_2.data(), num_schicht_test, omega_c0_test, k_ix_omega_c0_2.data());
    calc_TransferMatrix(k_ix_omega_c0_2.data(), dn_thick_test.data(), M.data(), interim.data(), num_schicht_test);
    k_inx = k_ix_omega_c0_2.back().real();
    k_outx = k_ix_omega_c0_2[0].real();
    result = calc_RTP(M.data(), k_outx, k_inx);
    REQUIRE(result < 0.01);
}
